<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Kabir Panthy',
            'email' => 'kvr24611@gmail.com',
            'password' => Hash::make('sam123'),
            'age' => '26',
            'bio' => 'Hello World..',
        ]);
    }
}
